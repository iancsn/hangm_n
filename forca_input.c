// Funções responsáveis pela lógica, entrada e pelo processamento de dados da forca
void definir_palavra(){
  // Define a palavra que deve ser adivinhada
  FILE *arquivo;

  arquivo = fopen("palavras.txt", "r");

  if(arquivo != NULL){
    int numero_de_linhas = obter_numero_de_linhas_do_arquivo(arquivo);
    int numero_aleatorio = gerar_numero_aleatorio(1, numero_de_linhas);

    for(int linha=0; linha<numero_aleatorio; linha++){
      fscanf(arquivo,"%s\n", palavra_definida);
    }
    rewind(arquivo);
  }
  else{
    printf("Houve um erro ao tentar abrir o arquivo.\nVerifique o caminho/nome ou se o arquivo existe.\n");
  }
  fclose(arquivo);
}

void preencher_forca(){
  if(total_de_erros == 1){
    cabeca = 'O';
  }
  if(total_de_erros == 2){
    braco_esquerdo = '/';
    braco_direito = '\\';
  }
  if(total_de_erros == 3){
    corpo = '|';
  }
  if(total_de_erros == 4){
    perna_esquerda = '/';
    perna_direita = '\\';
  }
}

void registrar_tentativa(){
  for(int indice=0; indice<TAMANHO_DA_PALAVRA; indice++){
    if(tentativas[indice] == '\0'){
      tentativas[indice] = letra_informada_pelo_jogador;
      break;
    }
  }
}

void registrar_tentativa_errada(){
  total_de_erros += 1;
  for(int indice=0; indice<TAMANHO_DA_PALAVRA; indice++){
    if(tentativas_erradas[indice] == '\0'){
      tentativas_erradas[indice] = letra_informada_pelo_jogador;
      break;
    }
  }
}

void registrar_tentativa_certa(int acertos){
  total_de_acertos += acertos;
  for(int indice=0; indice<strlen(palavra_definida); indice++){
    if(letra_informada_pelo_jogador == palavra_definida[indice]){
      tentativas_certas[indice] = letra_informada_pelo_jogador;
    }
    else{
      if(tentativas_certas[indice] == '\0'){
        tentativas_certas[indice] = '_';
      }
    }
  }
}

void verificar_tentiva(){
  int acertos = 0;
  if(!tentou()){
    registrar_tentativa();
    for(int indice=0; indice<strlen(palavra_definida); indice++){
      if(letra_informada_pelo_jogador == palavra_definida[indice]){
        acertos += 1;
      }
    }
    if(acertos >= 1){
      registrar_tentativa_certa(acertos);
    }
    else{
      registrar_tentativa_errada();
    }
  }
}

void informar_letra(){
  char letra_auxiliar;
  printf("\nInforme uma letra: ");
  scanf(" %c", &letra_auxiliar);
  letra_informada_pelo_jogador = tolower(letra_auxiliar);
  verificar_tentiva();
}
