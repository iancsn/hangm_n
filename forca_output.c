// Funções responsáveis pelo output da interface e dos dados do jogo
void imprimir_tentativas(){
  printf("\n\nTentativas: ");
  for(int indice=0; indice <TAMANHO_DA_PALAVRA; indice++){
    printf("%c ", tentativas[indice]);
  }
}

void imprimir_tentativas_certas(){
  if(tentativas_certas[0] != '\0'){
    for(int indice=0; indice<TAMANHO_DA_PALAVRA; indice++){
      printf("%c ", tentativas_certas[indice]);
    }
  }
  else{
    for(int indice=0; indice<strlen(palavra_definida); indice++){
      printf("_ ");
    }
  }
}

void imprimir_tentativas_erradas(){
  printf("\nTentativas erradas: ");
  for(int indice=0; indice <TAMANHO_DA_PALAVRA; indice++){
    printf("%c ", tentativas_erradas[indice]);
  }
  printf("\nTotal de Erros: %i\n", total_de_erros);
}

void imprimir_forca(){
  system("clear");

  printf(" _____\n");
  printf("|     |");
  printf("\n|     %c", cabeca);

  if(corpo != '\0'){
    printf("\n|    %c%c%c", braco_esquerdo, corpo, braco_direito);
  }
  else{
    printf("\n|    %c %c", braco_esquerdo, braco_direito);
  }
  printf("\n|    %c %c\n\n", perna_esquerda, perna_direita);


  //printf(palavra_definida);
  printf("\n");
  imprimir_tentativas_certas();
  imprimir_tentativas();
  imprimir_tentativas_erradas();

  if(perdeu()){
    printf("\nPuts... Voce perdeu!\n");
  }
  if(ganhou()){
    printf("\nParabens, voce venceu! \n");
  }
}
