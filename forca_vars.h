#define TAMANHO_DO_NOME_DO_ARQUIVO 30
#define TAMANHO_DA_PALAVRA 30

char nome_do_arquivo[TAMANHO_DO_NOME_DO_ARQUIVO];
char palavra_definida[TAMANHO_DA_PALAVRA];
char tentativas[TAMANHO_DA_PALAVRA];
char tentativas_erradas[TAMANHO_DA_PALAVRA];
char tentativas_certas[TAMANHO_DA_PALAVRA];
char letra_informada_pelo_jogador;
char cabeca, corpo, braco_esquerdo, braco_direito, perna_esquerda, perna_direita;
int total_de_acertos = 0;
int total_de_erros = 0;
int letra_ja_informada = 0;
